;;=======================================================================;;
;;========= NES Assembly code by François (BinarMorker) Allard ==========;;
;;=======================================================================;;
;;
;; $0000-0800 - Internal RAM, 2KB chip in the NES
;; $2000-2007 - PPU access ports
;; $4000-4017 - Audio and controller access ports
;; $6000-7FFF - Optional WRAM inside the game cart
;; $8000-FFFF - Game cart ROM
;;
;; SPRITEDATA
;; 76543210
;; |||   ||
;; |||   ++- Color Palette of sprite.  Choose which set of 4 from the 16 colors to use
;; ||+------ Priority (0: in front of background; 1: behind background)
;; |+------- Flip sprite horizontally
;; +-------- Flip sprite vertically
;;
;; JOYPADDATA
;; 76543210
;; ||||||||
;; |||||||+- Right
;; ||||||+-- Left
;; |||||+--- Down
;; ||||+---- Up
;; |||+----- Start
;; ||+------ Select
;; |+------- B
;; +-------- A
;;
;; PPUCTRL ($2000)
;; 76543210
;; | ||||||
;; | ||||++- Base nametable address
;; | ||||    (0 = $2000; 1 = $2400; 2 = $2800; 3 = $2C00)
;; | |||+--- VRAM address increment per CPU read/write of PPUDATA
;; | |||     (0: increment by 1, going across; 1: increment by 32, going down)
;; | ||+---- Sprite pattern table address for 8x8 sprites (0: $0000; 1: $1000)
;; | |+----- Background pattern table address (0: $0000; 1: $1000)
;; | +------ Sprite size (0: 8x8; 1: 8x16)
;; +-------- Generate an NMI at the start of the
;;           vertical blanking interval vblank (0: off; 1: on)
;;
;; PPUMASK ($2001)
;; 76543210
;; ||||||||
;; |||||||+- Grayscale (0: normal color; 1: AND all palette entries
;; |||||||   with 0x30, effectively producing a monochrome display;
;; |||||||   note that colour emphasis STILL works when this is on!)
;; ||||||+-- Disable background clipping in leftmost 8 pixels of screen
;; |||||+--- Disable sprite clipping in leftmost 8 pixels of screen
;; ||||+---- Enable background rendering
;; |||+----- Enable sprite rendering
;; ||+------ Intensify reds (and darken other colors)
;; |+------- Intensify greens (and darken other colors)
;; +-------- Intensify blues (and darken other colors)
;;
;;=======================================================================;;

  .inesprg 1  ; 1x 16KB bank of PRG code
  .ineschr 1  ; 1x 8KB bank of CHR data
  .inesmap 0  ; Mapper 0 = NROM, no bank swapping
  .inesmir 1  ; Background mirroring

;;------------------------------ VARIABLES ------------------------------;;

  .rsset $0000             ; Put variables starting at 0

gamestate     .rs 1        ; $0000 -- State of the game (0 = Title screen / 1 = Game screen)
loadstate     .rs 1        ; $0001 -- Has data loaded or not (1 / 0)
controller1   .rs 1        ; $0002 -- Put controller data for player 1
controller2   .rs 1        ; $0003 -- Put controller data for player 2
pttable       .rs 2        ; $0004 -- Pointer to the currently used palette table
sptable       .rs 2        ; $0006 -- Pointer to the currently used sprites table
bgtable       .rs 2        ; $0008 -- Pointer to the currently used background table
bgtable2      .rs 2        ; $000A -- Pointer to the currently used background table
bgtable3      .rs 2        ; $000C -- Pointer to the currently used background table
bgtable4      .rs 2        ; $000E -- Pointer to the currently used background table
attable       .rs 2        ; $0010 -- Pointer to the currently used attribute table
spnum         .rs 1        ; $0012 -- Number of sprites to load
spnumd2       .rs 1        ; $0013 -- Number of sprites divided by two
p2sprnum      .rs 2        ; $0014 -- The address for player 2's character
nexttileX     .rs 1        ; $0016 -- X position of the adjacent tile for which we check collision
nexttileY     .rs 1        ; $0017 -- Y position of the adjacent tile for which we check collision
calctileY     .rs 1        ; $0018 -- Y tile position rotated to the left by 32
posindex      .rs 2        ; $0019 -- Total size of the position index
hascollided   .rs 1        ; $001B -- Check if a player collision has occurred
anitimer      .rs 1        ; $001C -- Timer used for animations
aniframe      .rs 1        ; $001D -- Current animation frame
frameinc      .rs 1        ; $001E -- Frame increment to jump to next sprite
frameincx4    .rs 1        ; $001F -- Frame increment to jump to next sprite x4
attributepos  .rs 6        ; $0020 -- 3 first bytes for tile, attribute and byte X position, 3 last bytes for Y position
curpalette    .rs 1        ; $0026 -- Current palette for the 2x2 tile group the player is on
attribute     .rs 64       ; $0027 -- Attribute copied in RAM
startpressed  .rs 2        ; $0067 -- Detects if start is still pressed
movpal        .rs 1        ; $0069 -- Current palette for moving player
attrflag      .rs 2        ; $006A -- Flag to check if we need to refresh the attribute table
spnumnoanim   .rs 1        ; $006C -- Total number of sprites, including static ones
attrtoggle    .rs 1        ; $006D -- Toggle to refresh either attribute 1 or 2
chkattr       .rs 1        ; $006E -- Current Attribute value being tested for score
score         .rs 4        ; $006F -- Each player's score
timer         .rs 1        ; $0073 -- Time remaining before Game Over
player1movement  .rs 1     ; $0074
player2movement  .rs 1     ; $0075

;;------------------------------ CONSTANTS ------------------------------;;

STATETITLE     = $00        ; Displaying title screen
STATEPLAYING   = $01        ; Displaying game screen
STATEGAMEOVER  = $02        ; Displaying gameover screen
PPUCTRL        = %10001000  ; Enable NMI, sprites from Pattern Table 1, backgrounds from Pattern Table 0
PPUMASK        = %00011110  ; Enable sprites, enable background, no clipping on left side
DIRUP          = $01
DIRDOWN        = $02
DIRLEFT        = $04
DIRRIGHT       = $08

;;=======================================================================;;
;;================================ BANK 0 ===============================;;
;;=======================================================================;;

  .bank 0
  
;;=======================================================================;;
;;============================== CODE START =============================;;
;;=======================================================================;;

  .org $C000
  .code

;;------------------------------ RESET STUFF ----------------------------;;

RESET:
  SEI         ; Disable IRQs
  CLD         ; Disable decimal mode
  LDX #$40
  STX $4017   ; Disable APU frame IRQ
  LDX #$FF
  TXS         ; Set up stack
  INX         ; Now X = 0
  STX $2000   ; Disable NMI
  STX $2001   ; Disable rendering
  STX $4010   ; Disable DMC IRQs
  JSR vblankwait

clrmem:       ; Clears all memory to make sure everything is properly reset
  LDA #$00
  STA $0000, x
  STA $0100, x
  STA $0300, x
  STA $0400, x
  STA $0500, x
  STA $0600, x
  STA $0700, x
  LDA #$FF
  STA $0200, x
  INX
  BNE clrmem
  JSR vblankwait

;;-------------------------- VARS INITIALIZATION ------------------------;;

VarsInit:
  LDA #STATETITLE
  STA gamestate
  LDA #$00
  STA loadstate
  JSR PPUInit

;;=======================================================================;;
;;============================= GAME ENGINE =============================;;
;;=======================================================================;;

MainLoop:
  JSR ReadController1   ; Get the current button data for Player 1
  JSR ReadController2   ; Get the current button data for Player 2

GameEngine:
  LDA gamestate
  CMP #STATETITLE
  BNE NotTitleScreen    ; Game is displaying Title Screen
  JMP EngineTitle
  
NotTitleScreen:
  LDA gamestate
  CMP #STATEPLAYING
  BNE NotGameScreen     ; Game is playing
  JMP EnginePlaying
  
NotGameScreen:
  LDA gamestate
  CMP #STATEGAMEOVER
  BNE NotGameOverScreen ; Game is displaying Game Over Screen
  JMP EngineGameOver
  
NotGameOverScreen:
  JMP GameEngineDone

;;----------------------------- TITLE SCREEN ----------------------------;;

EngineTitle:
  LDA controller1       ; Load the current button state for controller1
  AND #%00010000        ; Isolate the bit representing "start", by clearing all the other bits
  BEQ AStartUnpress1
  LDX startpressed
  CPX #$01
  BEQ AStartCheck2
  LDA #STATEPLAYING
  STA gamestate
  LDA #$00
  STA loadstate
  LDX #$01
  STX startpressed
  JMP ACheckDone
  
AStartUnpress1:
  LDX #$00
  STX startpressed
  
AStartCheck2:
  LDA controller2       ; Load the current button state for controller1
  AND #%00010000        ; Isolate the bit representing "start", by clearing all the other bits
  BEQ AStartUnpress2
  LDX startpressed+1
  CPX #$01
  BEQ ACheckDone
  LDA #STATEPLAYING
  STA gamestate
  LDA #$00
  STA loadstate
  LDX #$01
  STX startpressed+1
  JMP ACheckDone
  
AStartUnpress2:
  LDX #$00
  STX startpressed+1
  
ACheckDone:
  JMP GameEngineDone
  
;;--------------------------- MAIN GAME SCREEN --------------------------;;

EnginePlaying:
  LDA #$00
  STA player1movement
  STA player2movement
  LDA controller1       ; Load the current button state for controller1
  AND #%00010000        ; Isolate the bit representing "select", by clearing all the other bits
  BEQ BStartUnpress1
  LDX startpressed
  CPX #$01
  BEQ BStartCheck2
  LDA #STATEGAMEOVER
  STA gamestate
  LDA #$00
  STA loadstate
  LDX #$01
  STX startpressed
  JMP BCheckDone
  
BStartUnpress1:
  LDX #$00
  STX startpressed
  
BStartCheck2:
  LDA controller2       ; Load the current button state for controller1
  AND #%00010000        ; Isolate the bit representing "select", by clearing all the other bits
  BEQ BStartUnpress2
  LDX startpressed+1
  CPX #$01
  BEQ BUpCheck1
  LDA #STATEGAMEOVER
  STA gamestate
  LDA #$00
  STA loadstate
  LDX #$01
  STX startpressed+1
  JMP BCheckDone
  
BStartUnpress2:
  LDX #$00
  STX startpressed+1
  
BUpCheck1:
  LDA controller1       ; Load the current button state for controller1
  AND #%00001000        ; Isolate the bit representing "up", by clearing all the other bits
  BEQ BDownCheck1
  LDA #DIRUP
  STA player1movement
  JMP BUpCheck2

BDownCheck1:
  LDA controller1       ; Load the current button state for controller1
  AND #%00000100        ; Isolate the bit representing "down", by clearing all the other bits
  BEQ BLeftCheck1
  LDA #DIRDOWN
  STA player1movement
  JMP BUpCheck2

BLeftCheck1:
  LDA controller1       ; Load the current button state for controller1
  AND #%00000010        ; Isolate the bit representing "left", by clearing all the other bits
  BEQ BRightCheck1
  LDA #DIRLEFT
  STA player1movement
  JMP BUpCheck2

BRightCheck1:
  LDA controller1       ; Load the current button state for controller1
  AND #%00000001        ; Isolate the bit representing "right", by clearing all the other bits
  BEQ BUpCheck2
  LDA #DIRRIGHT
  STA player1movement
  
BUpCheck2:
  LDA controller2       ; Load the current button state for controller1
  AND #%00001000        ; Isolate the bit representing "up", by clearing all the other bits
  BEQ BDownCheck2
  LDA #DIRUP
  STA player2movement
  JMP BCheckDone

BDownCheck2:
  LDA controller2       ; Load the current button state for controller1
  AND #%00000100        ; Isolate the bit representing "down", by clearing all the other bits
  BEQ BLeftCheck2
  LDA #DIRDOWN
  STA player2movement
  JMP BCheckDone

BLeftCheck2:
  LDA controller2       ; Load the current button state for controller1
  AND #%00000010        ; Isolate the bit representing "left", by clearing all the other bits
  BEQ BRightCheck2
  LDA #DIRLEFT
  STA player2movement
  JMP BCheckDone

BRightCheck2:
  LDA controller2       ; Load the current button state for controller1
  AND #%00000001        ; Isolate the bit representing "right", by clearing all the other bits
  BEQ BCheckDone
  LDA #DIRRIGHT
  STA player2movement
  
BCheckDone:
  JMP GameEngineDone

;;--------------------------- GAME OVER SCREEN --------------------------;;

EngineGameOver:
  LDA controller1       ; Load the current button state for controller1
  AND #%00010000        ; Isolate the bit representing "start", by clearing all the other bits
  BEQ CStartUnpress1
  LDX startpressed
  CPX #$01
  BEQ CStartCheck2
  LDA #STATETITLE
  STA gamestate
  LDA #$00
  STA loadstate
  LDX #$01
  STX startpressed
  JMP CCheckDone
  
CStartUnpress1:
  LDX #$00
  STX startpressed
  
CStartCheck2:
  LDA controller2       ; Load the current button state for controller1
  AND #%00010000        ; Isolate the bit representing "start", by clearing all the other bits
  BEQ CStartUnpress2
  LDX startpressed+1
  CPX #$01
  BEQ CCheckDone
  LDA #STATETITLE
  STA gamestate
  LDA #$00
  STA loadstate
  LDX #$01
  STX startpressed+1
  JMP CCheckDone
  
CStartUnpress2:
  LDX #$00
  STX startpressed+1
  
CCheckDone:
  
GameEngineDone:
  JMP MainLoop            ; Jump back to MainLoop, infinite loop
  
;;=======================================================================;;
;;================================= NMI =================================;;
;;=======================================================================;;
  
NMI:
  PHP
  PHA
  TXA
  PHA
  TYA
  PHA
  LDA #$00
  STA $2003               ; Set the low byte (00) of the RAM address
  LDA #$02
  STA $4014               ; Set the high byte (02) of the RAM address, start the transfer
  JSR PPUInit
  
LoadEngine:
  LDA gamestate
  CMP #STATETITLE
  BNE NotTitleLoad    ; Game is displaying Title Screen
  JMP LoadTitle
  
NotTitleLoad:
  LDA gamestate
  CMP #STATEPLAYING
  BNE NotGameLoad     ; Game is playing
  JMP LoadPlaying
  
NotGameLoad:
  LDA gamestate
  CMP #STATEGAMEOVER
  BNE NotGameOverLoad ; Game is displaying Game Over Screen
  JMP LoadGameOver
  
NotGameOverLoad:
  JMP GameLoadDone

;;----------------------------- TITLE SCREEN ----------------------------;;

LoadTitle:
  LDA loadstate
  AND #$01
  BNE NoLoadTitle
  JSR PPUStop
  LDA #LOW(TitlePalette)
  STA pttable
  LDA #HIGH(TitlePalette)
  STA pttable+1
  LDA TitleSpritesNum
  STA spnumnoanim
  LDA #LOW(TitleSprites)
  STA sptable
  LDA #HIGH(TitleSprites)
  STA sptable+1
  LDA #LOW(TitleBackground)
  STA bgtable
  LDA #HIGH(TitleBackground)
  STA bgtable+1
  LDA #LOW(TitleBackground+256)
  STA bgtable2
  LDA #HIGH(TitleBackground+256)
  STA bgtable2+1
  LDA #LOW(TitleBackground+512)
  STA bgtable3
  LDA #HIGH(TitleBackground+512)
  STA bgtable3+1
  LDA #LOW(TitleBackground+768)
  STA bgtable4
  LDA #HIGH(TitleBackground+768)
  STA bgtable4+1
  LDA #LOW(TitleAttribute)
  STA attable
  LDA #HIGH(TitleAttribute)
  STA attable+1
  JSR PPURestart
  
NoLoadTitle:
  JMP GameLoadDone

;;--------------------------- MAIN GAME SCREEN --------------------------;;

LoadPlaying:
  LDA loadstate
  AND #$01
  BNE NoLoadPlaying
  JSR PPUStop
  LDA #LOW(GamePalette)
  STA pttable
  LDA #HIGH(GamePalette)
  STA pttable+1
  LDA GameSpritesNum
  STA spnumnoanim
  LDA GameSpritesNum+1
  STA spnum
  LDA GameSpritesNum+2
  STA spnumd2
  LDA #LOW(GameSprites)
  STA sptable
  LDA #HIGH(GameSprites)
  STA sptable+1
  LDA #LOW(GameBackground)
  STA bgtable
  LDA #HIGH(GameBackground)
  STA bgtable+1
  LDA #LOW(GameBackground+256)
  STA bgtable2
  LDA #HIGH(GameBackground+256)
  STA bgtable2+1
  LDA #LOW(GameBackground+512)
  STA bgtable3
  LDA #HIGH(GameBackground+512)
  STA bgtable3+1
  LDA #LOW(GameBackground+768)
  STA bgtable4
  LDA #HIGH(GameBackground+768)
  STA bgtable4+1
  LDA #LOW(GameAttribute)
  STA attable
  LDA #HIGH(GameAttribute)
  STA attable+1
  JSR PPURestart
  
NoLoadPlaying:
  JMP GameLoadDone
  
;;--------------------------- GAME OVER SCREEN --------------------------;;

LoadGameOver:
  LDA loadstate
  AND #$01
  BNE GameLoadDone
  JSR PPUStop
  LDA #LOW(GameOverPalette)
  STA pttable
  LDA #HIGH(GameOverPalette)
  STA pttable+1
  LDA GameOverSpritesNum
  STA spnumnoanim
  LDA #LOW(GameOverSprites)
  STA sptable
  LDA #HIGH(GameOverSprites)
  STA sptable+1
  LDA #LOW(GameOverBackground)
  STA bgtable
  LDA #HIGH(GameOverBackground)
  STA bgtable+1
  LDA #LOW(GameOverBackground+256)
  STA bgtable2
  LDA #HIGH(GameOverBackground+256)
  STA bgtable2+1
  LDA #LOW(GameOverBackground+512)
  STA bgtable3
  LDA #HIGH(GameOverBackground+512)
  STA bgtable3+1
  LDA #LOW(GameOverBackground+768)
  STA bgtable4
  LDA #HIGH(GameOverBackground+768)
  STA bgtable4+1
  LDA #LOW(GameOverAttribute)
  STA attable
  LDA #HIGH(GameOverAttribute)
  STA attable+1
  JSR PPURestart
  
;;--------------------------------- RTI ---------------------------------;;

GameLoadDone:
  LDA player1movement
  CMP #DIRUP
  BNE NotP1Up
  JSR CheckCollision1Up
  LDA hascollided
  AND #$01
  BNE EndMovementP1
  LDX #0
  JSR BMovePlayer1Up
  JMP EndMovementP1
  
NotP1Up:
  LDA player1movement
  CMP #DIRDOWN
  BNE NotP1Down
  JSR CheckCollision1Down
  LDA hascollided
  AND #$01
  BNE EndMovementP1
  LDX #0
  JSR BMovePlayer1Down
  JMP EndMovementP1
  
NotP1Down:
  LDA player1movement
  CMP #DIRLEFT
  BNE NotP1Left
  JSR CheckCollision1Left
  LDA hascollided
  AND #$01
  BNE EndMovementP1
  LDX #0
  JSR BMovePlayer1Left
  JMP EndMovementP1
  
NotP1Left:
  LDA player1movement
  CMP #DIRRIGHT
  BNE EndMovementP1
  JSR CheckCollision1Right
  LDA hascollided
  AND #$01
  BNE EndMovementP1
  LDX #0
  JSR BMovePlayer1Right
  
EndMovementP1:
  LDA player2movement
  CMP #DIRUP
  BNE NotP2Up
  JSR CheckCollision2Up
  LDA hascollided+1
  AND #$01
  BNE EndMovementP1
  LDX #0
  JSR BMovePlayer2Up
  JMP EndMovementP2
  
NotP2Up:
  LDA player2movement
  CMP #DIRDOWN
  BNE NotP2Down
  JSR CheckCollision2Down
  LDA hascollided+1
  AND #$01
  BNE EndMovementP1
  LDX #0
  JSR BMovePlayer2Down
  JMP EndMovementP2
  
NotP2Down:
  LDA player2movement
  CMP #DIRLEFT
  BNE NotP2Left
  JSR CheckCollision2Left
  LDA hascollided+1
  AND #$01
  BNE EndMovementP1
  LDX #0
  JSR BMovePlayer2Left
  JMP EndMovementP2
  
NotP2Left:
  LDA player2movement
  CMP #DIRRIGHT
  BNE EndMovementP2
  JSR CheckCollision2Right
  LDA hascollided+1
  AND #$01
  BNE EndMovementP1
  LDX #0
  JSR BMovePlayer2Right
  
EndMovementP2:
  LDX anitimer
  INX
  STX anitimer
  CPX #$10
  BNE EndAnimation
  JSR DoAnimation
  
EndAnimation:
  LDX attrflag
  CPX #$FF
  BEQ NoP1Refresh
  JSR RefreshAttribute1
  LDX #0
  STX attrflag

NoP1Refresh:
  LDX attrflag+1
  CPX #$FF
  BEQ NoP2Refresh
  JSR RefreshAttribute2
  LDX #0
  STX attrflag+1
  
NoP2Refresh:
  PLA
  TAY
  PLA
  TAX
  PLA
  PLP
  RTI                   ; Return from interrupt
  
;;=======================================================================;;
;;============================== SUBROUTINES ============================;;
;;=======================================================================;;

  .include "subroutines.inc"
  
;;=======================================================================;;
;;================================ BANK 1 ===============================;;
;;=======================================================================;;

  .bank 1
  .org $E000
  
;;------------------------------ ANIMATIONS -----------------------------;;

AniSprite1:   
  .db $02,$00,$01,$00,$01,$0A,$08,$09,$08,$09
  
AniSprite2:   
  .db $05,$03,$04,$03,$04,$10,$0E,$0F,$0E,$0F
  
AniSprite3:   
  .db $02,$00,$01,$00,$01,$0A,$08,$09,$08,$09
  
AniSprite4:   
  .db $05,$03,$04,$03,$04,$0D,$0B,$0C,$0B,$0C
  
AniSprite5:   
  .db $10,$06,$07,$06,$07,$0A,$08,$09,$08,$09
  
AniSprite6:   
  .db $05,$03,$04,$03,$04,$0D,$0B,$0C,$0B,$0C
  
AniSprite7:   
  .db $02,$00,$01,$00,$01,$0A,$08,$09,$08,$09
  
AniSprite8:   
  .db $05,$03,$04,$03,$04,$0D,$0B,$0C,$0B,$0C

;;------------------------------- PALETTES ------------------------------;;

GamePalette:  ; Goes as: Palette#1 (Transp, Col1, Col2, Col3), Palette#2 (Transp, Col1, Col2, Col3) ...
  .db $0F,$29,$18,$39, $0F,$22,$12,$32, $0F,$16,$06,$26, $0F,$0B,$1B,$2B  ; Background palette
  .db $0F,$12,$22,$32, $0F,$06,$16,$26, $0F,$0F,$16,$30, $0F,$0F,$22,$30  ; Sprite palette
  
TitlePalette:  ; Goes as: Palette#1 (Transp, Col1, Col2, Col3), Palette#2 (Transp, Col1, Col2, Col3) ...
  .db $0F,$10,$0F,$10, $0F,$12,$0F,$12, $0F,$06,$0F,$06, $0F,$39,$0F,$39   ; Background palette
  .db $0F,$0F,$0F,$0F, $0F,$0F,$0F,$0F, $0F,$0F,$0F,$0F, $0F,$0F,$0F,$0F   ; Sprite palette
  
GameOverPalette:  ; Goes as: Palette#1 (Transp, Col1, Col2, Col3), Palette#2 (Transp, Col1, Col2, Col3) ...
  .db $0F,$10,$0F,$10, $0F,$12,$0F,$12, $0F,$06,$0F,$06, $0F,$39,$0F,$39   ; Background palette
  .db $0F,$12,$22,$32, $0F,$06,$16,$26, $0F,$0F,$16,$30, $0F,$0F,$22,$30  ; Sprite palette

;;------------------------------- SPRITES -------------------------------;;

GameSpritesNum:
  .db $44, $28, $14

GameSprites:  ; Goes as: PosY, Num, Attribute, PosX
  .db $7C, $02, %00000010, $3C
  .db $78, $00, %00000000, $38
  .db $80, $01, %00000000, $38
  .db $78, $00, %01000000, $40
  .db $80, $01, %01000000, $40
  
  .db $7C, $0A, %00000011, $BC
  .db $78, $08, %00000001, $B8
  .db $80, $09, %00000001, $B8
  .db $78, $08, %01000001, $C0
  .db $80, $09, %01000001, $C0
  
  .db $0F, $11, %00000000, $48
  .db $0F, $11, %00000000, $50
  .db $0F, $11, %00000010, $70
  .db $0F, $11, %00000010, $80
  .db $0F, $11, %00000010, $88
  .db $0F, $11, %00000001, $E8
  .db $0F, $11, %00000001, $F0
  
TitleSpritesNum:
  .db $4
  
TitleSprites:  ; Goes as: PosY, Num, Attribute, PosX
  .db $FF, $00, %00000000, $FF
  
GameOverSpritesNum:
  .db $14
  
GameOverSprites:  ; Goes as: PosY, Num, Attribute, PosX
  .db $7C, $02, %00000010, $7C
  .db $78, $00, %00000000, $78
  .db $80, $01, %00000000, $78
  .db $78, $00, %01000000, $80
  .db $80, $01, %01000000, $80

;;------------------------------ BACKGROUND -----------------------------;;

  .include "background.inc"

;;----------------------------- INTERRUPTIONS ---------------------------;;

  .org $FFFA  ; First of the three vectors starts here
  .dw NMI     ; When an NMI happens (once per frame if enabled) the processor will jump to the label NMI:
  .dw RESET   ; When the processor first turns on or is reset, it will jump to the label RESET:
  .dw 0       ; External interrupt IRQ is not used now

;;=======================================================================;;
;;================================ BANK 2 ===============================;;
;;=======================================================================;;

  .bank 2

;;------------------------------ RESSOURCES -----------------------------;;

  .include "ressources.inc"
  
;;=======================================================================;;
;;=============================== CODE END ==============================;;
;;=======================================================================;;
