;;----------------------------- LOAD PALETTE ----------------------------;;

LoadPalettes:
  LDA $2002               ; Read PPU status to reset the high/low latch
  LDA #$3F
  STA $2006               ; Write the high byte of $3F00 address
  LDA #$00
  STA $2006               ; Write the low byte of $3F00 address
  LDY #$00

LoadPalettesLoop:
  LDA [pttable], y      ; Load data from address (palette + the value in Y)
  STA $2007               ; Write to PPU
  INY                     ; Set index to next byte
  CPY #$20
  BNE LoadPalettesLoop    ; If Y = $20, 32 bytes copied, all done
  RTS

;;----------------------------- LOAD SPRITES ----------------------------;;

LoadSprites:
  LDY #$00                ; Start at 0

LoadSpritesLoop:
  LDA [sptable], y        ; Load data from address (Sprites + Y)
  STA $0200, y            ; Store into RAM address ($0200 + Y)
  INY                     ; X = X + 1
  CPY spnumnoanim               ; Compare Y to hex $04, decimal 04
  BNE LoadSpritesLoop     ; Branch to LoadSpritesLoop if compare was Not Equal to zero
  RTS

;;---------------------------- LOAD BACKGROUND --------------------------;;

LoadBackground:
  LDA $2002               ; Read PPU status to reset the high/low latch
  LDA #$20
  STA $2006               ; Write the high byte
  LDA #$00
  STA $2006               ; Write the low byte
  LDY #$00

LoadBackgroundLoop:
  LDA [bgtable], y   ; Load the value
  STA $2007               ; Store it to the PPU at the address we previously specified
  INY                     ; Next Y value
  CPY #$00
  BNE LoadBackgroundLoop  ; If Y flips over back to zero, we need to change our pointer values

LoadBackgroundLoop2:
  LDA [bgtable2], y   ; Load the value
  STA $2007               ; Store it to the PPU at the address we previously specified
  INY                     ; Next Y value
  CPY #$00
  BNE LoadBackgroundLoop2  ; If Y flips over back to zero, we need to change our pointer values

LoadBackgroundLoop3:
  LDA [bgtable3], y   ; Load the value
  STA $2007               ; Store it to the PPU at the address we previously specified
  INY                     ; Next Y value
  CPY #$00
  BNE LoadBackgroundLoop3  ; If Y flips over back to zero, we need to change our pointer values

LoadBackgroundLoop4:
  LDA [bgtable4], y   ; Load the value
  STA $2007               ; Store it to the PPU at the address we previously specified
  INY                     ; Next Y value
  CPY #$C0
  BNE LoadBackgroundLoop4  ; If Y flips over back to zero, we need to change our pointer values
  RTS

;;----------------------------- LOAD ATTRIBUTE --------------------------;;

InitAttribute:
  LDY #$00                ; Start out at 0

InitAttributeLoop:
  LDA [attable], y        ; Load data from address (attribute + the value in x)
  STA attribute, y        ; Write to PPU
  INY                     ; X = X + 1
  CPY #$40                ; Compare X to hex $78, decimal 120
  BNE InitAttributeLoop   ; Branch to InitAttributeLoop if compare was Not Equal to zero
  RTS
  
LoadAttribute:
  LDA $2002               ; Read PPU status to reset the high/low latch
  LDA #$23
  STA $2006               ; Write the high byte of $23C0 address
  LDA #$C0
  STA $2006               ; Write the low byte of $23C0 address
  LDY #$00                ; Start out at 0

LoadAttributeLoop:
  LDA attribute, y        ; Load data from address (attribute + the value in x)
  STA $2007               ; Write to PPU
  INY                     ; X = X + 1
  CPY #$40                ; Compare X to hex $78, decimal 120
  BNE LoadAttributeLoop   ; Branch to LoadAttributeLoop if compare was Not Equal to zero
  RTS
  
RefreshAttribute1:
  LDA $2002
  LDA #$C0
  CLC
  ADC attrflag
  TAY
  LDA #$23
  ADC #$00
  STA $2006
  STY $2006
  LDA attribute, x
  STA $2007
  JSR ScrollDisable
  RTS

RefreshAttribute2:
  LDA $2002
  LDA #$C0
  CLC
  ADC attrflag+1
  TAY
  LDA #$23
  ADC #$00
  STA $2006
  STY $2006
  LDA attribute, x
  STA $2007
  JSR ScrollDisable
  RTS

;;------------------------------- PPU STUFF -----------------------------;;

PPUStop:
  BIT $2002               ; Clear the VBL flag if it was set at reset time
  LDA #0
  STA $2000
  STA $2001
  RTS
  
PPUInit:
  BIT $2002               ; Clear the VBL flag if it was set at reset time
  LDA #PPUCTRL
  STA $2000
  LDA #PPUMASK
  STA $2001
  JSR ScrollDisable
  RTS

ClearSprites:       ; Clears all memory to make sure everything is properly reset
  LDA #$FE
  STA $0200, x
  INX
  BNE ClearSprites
  JSR vblankwait
  RTS
  
PPURestart:
  JSR ClearSprites
  JSR LoadPalettes
  JSR LoadSprites
  JSR LoadBackground
  JSR InitAttribute
  JSR LoadAttribute
  JSR PPUInit
  LDA #$01
  STA loadstate
  RTS
  
ScrollDisable:
  LDA #%00000000          ; Disable scrolling. Please disable it, dear NES. Please!
  STA $2006
  STA $2005
  STA $2005
  STA $2006
  RTS

vblankwait:               ; First wait for vblank to make sure PPU is ready
  BIT $2002
  BPL vblankwait
  RTS

;;------------------------------ ANIMATIONS -----------------------------;;

DoAnimation:
  LDX #$00
  STX frameinc
  STX frameincx4
  STX anitimer

LoadFrames:
  LDY aniframe
  CPY #$00
  BEQ LoadFrame1
  CPY #$02
  BEQ LoadFrame2
  CPY #$03
  BEQ LoadFrame3
  CPY #$04
  BEQ LoadFrame4
  CPY #$05
  BEQ LoadFrame5
  CPY #$06
  BEQ LoadFrame6
  CPY #$07
  BEQ LoadFrame7
  LDX frameinc
  LDA AniSprite8,x
  LDX frameincx4
  STA $0201,x
  JMP IncrementFrame
  
LoadFrame1:
  LDX frameinc
  LDA AniSprite1,x
  LDX frameincx4
  STA $0201,x
  JMP IncrementFrame
  
LoadFrame2:
  LDX frameinc
  LDA AniSprite2,x
  LDX frameincx4
  STA $0201,x
  JMP IncrementFrame
  
LoadFrame3:
  LDX frameinc
  LDA AniSprite3,x
  LDX frameincx4
  STA $0201,x
  JMP IncrementFrame
  
LoadFrame4:
  LDX frameinc
  LDA AniSprite4,x
  LDX frameincx4
  STA $0201,x
  JMP IncrementFrame
  
LoadFrame5:
  LDX frameinc
  LDA AniSprite5,x
  LDX frameincx4
  STA $0201,x
  JMP IncrementFrame
  
LoadFrame6:
  LDX frameinc
  LDA AniSprite6,x
  LDX frameincx4
  STA $0201,x
  JMP IncrementFrame
  
LoadFrame7:
  LDX frameinc
  LDA AniSprite7,x
  LDX frameincx4
  STA $0201,x
  
IncrementFrame:
  LDY frameinc
  INY
  STY frameinc
  INX
  INX
  INX
  INX
  STX frameincx4
  CPX spnum
  BMI ReturnFrames
  LDY aniframe
  INY
  TYA
  AND #%00000111
  STA aniframe
  RTS
  
ReturnFrames:
  JMP LoadFrames

;;------------------------------ COLLISIONS -----------------------------;;
  
CheckCollision1Left:
  CLC
  LDA $0206
  ADC #$01
  AND #%00000011
  STA movpal
  LDA $0200
  CLC
  ADC #$04
  STA attributepos+3
  STA nexttileY
  LDA $0203
  STA nexttileX
  CLC
  ADC #$04
  STA attributepos
  JMP ChkWalkability
  
CheckCollision1Right:
  CLC
  LDA $0206
  ADC #$01
  AND #%00000011
  STA movpal
  LDA $0200
  CLC
  ADC #$04
  STA attributepos+3
  STA nexttileY
  LDA $0203
  CLC
  ADC #$04
  STA attributepos
  CLC
  ADC #$04
  STA nexttileX
  JMP ChkWalkability

CheckCollision1Up:
  CLC
  LDA $0206
  ADC #$01
  AND #%00000011
  STA movpal
  LDA $0203
  CLC
  ADC #$04
  STA attributepos
  STA nexttileX
  LDA $0200
  STA nexttileY
  CLC
  ADC #$04
  STA attributepos+3
  LDX #$00
  CMP #$27
  BEQ PassBehind1
  CMP #$D7
  BEQ PassOnTop1
  JMP ChkWalkability
  
CheckCollision1Down:
  CLC
  LDA $0206
  ADC #$01
  AND #%00000011
  STA movpal
  LDA $0203
  CLC
  ADC #$04
  STA attributepos
  STA nexttileX
  LDA $0200
  CLC
  ADC #$04
  STA attributepos+3
  CLC
  ADC #$04
  STA nexttileY
  LDX #$00
  CMP #$2C
  BEQ PassOnTop1
  CMP #$DC
  BEQ PassBehind1
  JMP ChkWalkability
  
PassBehind1:
  LDA $0202, X
  ORA #%00100000
  STA $0202, X
  INX
  INX
  INX
  INX
  TXA
  CMP spnumd2
  BNE PassBehind1
  JMP ChkWalkability
  
PassOnTop1:
  LDA $0202, X
  AND #%11011111
  STA $0202, X
  INX
  INX
  INX
  INX
  TXA
  CMP spnumd2
  BNE PassOnTop1
  JMP ChkWalkability
  
CheckCollision2Left:
  CLC
  LDA $021A
  ADC #$01
  AND #%00000011
  STA movpal
  LDA $0214
  CLC
  ADC #$04
  STA nexttileY
  STA attributepos+3
  LDA $0217
  STA nexttileX
  CLC
  ADC #$04
  STA attributepos
  JMP ChkWalkability
  
CheckCollision2Right:
  CLC
  LDA $021A
  ADC #$01
  AND #%00000011
  STA movpal
  LDA $0214
  CLC
  ADC #$04
  STA attributepos+3
  STA nexttileY
  LDA $0217
  CLC
  ADC #$04
  STA attributepos
  CLC
  ADC #$04
  STA nexttileX
  JMP ChkWalkability
  
CheckCollision2Up:
  CLC
  LDA $021A
  ADC #$01
  AND #%00000011
  STA movpal
  LDA $0217
  CLC
  ADC #$04
  STA nexttileX
  STA attributepos
  LDA $0214
  STA nexttileY
  CLC
  ADC #$04
  STA attributepos+3
  LDX #$00
  CMP #$27
  BEQ PassBehind2
  CMP #$D7
  BEQ PassOnTop2
  JMP ChkWalkability
  
CheckCollision2Down:
  CLC
  LDA $021A
  ADC #$01
  AND #%00000011
  STA movpal
  LDA $0217
  CLC
  ADC #$04
  STA attributepos
  STA nexttileX
  LDA $0214
  CLC
  ADC #$04
  STA attributepos+3
  CLC
  ADC #$04
  STA nexttileY
  LDX #$00
  CMP #$2C
  BEQ PassOnTop2
  CMP #$DC
  BEQ PassBehind2
  JMP ChkWalkability
  
PassBehind2:
  LDA $0216, X
  ORA #%00100000
  STA $0216, X
  INX
  INX
  INX
  INX
  TXA
  CMP spnumd2
  BNE PassBehind2
  JMP ChkWalkability
  
PassOnTop2:
  LDA $0216, X
  AND #%11011111
  STA $0216, X
  INX
  INX
  INX
  INX
  TXA
  CMP spnumd2
  BNE PassOnTop2
  JMP ChkWalkability
  
ChkWalkability:
  JSR CalculateAttribute
  LDA nexttileX
  LSR a
  LSR a
  LSR a
  STA nexttileX
  LDA nexttileY
  LSR a
  LSR a
  LSR a
  STA nexttileY
  LDX #$00
  
Multiply32:
  CLC
  ASL A
  ADC #$00
  INX
  CPX #$05
  BMI Multiply32
  STA calctileY
  AND #%11100000
  CLC
  ADC nexttileX
  STA posindex
  LDA calctileY
  AND #%00011111
  STA posindex+1
  CLC
  LDA posindex
  ADC #LOW(GameTilesMetadata)
  TAX
  LDA posindex+1
  ADC #HIGH(GameTilesMetadata)
  STA posindex+1
  STX posindex
  LDY #$00
  LDA [posindex],y
  CMP #$01
  BEQ FoundCollision
  
NoCollision:
  LDX #$00
  STX hascollided
  RTS
  
FoundCollision:
  LDX #$01
  STX hascollided
  RTS

;;------------------------------ GAME LOGIC -----------------------------;;

CalculateAttribute:
  LDA attributepos     ; Load Sprite X (Will become Tile X)
  LSR a
  LSR a
  LSR a
  STA attributepos     ; Stock Tile X (Sprite X / 8)
  LSR a
  LSR a
  STA attributepos+1   ; Stock Attribute X (Tile X / 4)
  LDA attributepos     ; Load Tile X again
  LSR a
  AND #%00000001
  STA attributepos+2   ; Stock Byte X (only the last bit)
  LDA attributepos+3   ; Load Sprite Y (Will become Tile Y)
  LSR a
  LSR a
  LSR a
  STA attributepos+3   ; Stock Tile Y (Sprite Y / 8)
  CMP #$05
  BMI NoLoadPalette
  LDA attributepos+3   ; Load Tile Y again after blanking out the upper part
  CMP #$1B
  BPL NoLoadPalette
  LDA attributepos+3   ; Load Tile Y again after blanking out the bottom part
  LSR a
  LSR a
  STA attributepos+4   ; Stock Attribute Y (Tile Y / 4)
  LDA attributepos+3   ; Load Tile Y again
  LSR a
  AND #%00000001
  STA attributepos+5   ; Stock Byte Y (only the last bit)
  LDA attributepos+4   ; Load Attribute Y
  ASL a
  ASL a
  ASL a
  CLC
  ADC attributepos+1   ; Stock Attribute index ((Attribute Y * 8) + Attribute X)
  TAX
  LDY attribute,x      ; Load corresponding attribute
  LDA attributepos+5
  ASL a
  CLC
  ADC attributepos+2
  JMP SelectPalette
  
NoLoadPalette:
  RTS
  
SelectPalette:
  STA curpalette
  CMP #$00
  BEQ IsAttributeUL
  LDA curpalette
  CMP #$01
  BEQ IsAttributeUR
  LDA curpalette
  CMP #$02
  BEQ IsAttributeBL

IsAttributeBR:
  TYA
  AND #%11000000
  LSR a
  LSR a
  LSR a
  LSR a
  LSR a
  LSR a
  LDA attribute,x      ; Load corresponding attribute
  AND #%00111111
  STA curpalette
  LDA movpal
  ASL a
  ASL a
  ASL a
  ASL a
  ASL a
  ASL a
  EOR curpalette
  STA attribute,x
  JMP EndLoadPalette
  
IsAttributeBL:
  TYA
  AND #%00110000
  LSR a
  LSR a
  LSR a
  LSR a
  LDA attribute,x      ; Load corresponding attribute
  AND #%11001111
  STA curpalette
  LDA movpal
  ASL a
  ASL a
  ASL a
  ASL a
  EOR curpalette
  STA attribute,x
  JMP EndLoadPalette

NoLoadPalette1:
  RTS
  
IsAttributeUL:
  TYA
  AND #%00000011
  LDA attribute,x      ; Load corresponding attribute
  AND #%11111100
  STA curpalette
  LDA movpal
  EOR curpalette
  STA attribute,x
  JMP EndLoadPalette
  
IsAttributeUR:
  TYA
  AND #%00001100
  LSR a
  LSR a
  LDA attribute,x      ; Load corresponding attribute
  AND #%11110011
  STA curpalette
  LDA movpal
  ASL a
  ASL a
  EOR curpalette
  STA attribute,x

EndLoadPalette:
  LDA movpal
  CMP #$00
  BEQ WritePaletteP2
  LDY #$FF
  STY attrflag+1
  STX attrflag
  RTS
  
WritePaletteP2:
  LDY #$FF
  STY attrflag 
  STX attrflag+1
  RTS

NoLoadPalette2:
  RTS
  
;;----------------------------- PLAYER 1 MOV ----------------------------;;

BMovePlayer1Up:
  LDA $0200, X          ; Load sprite Y position
  SEC                   ; Make sure the carry flag is clear
  SBC #$01              ; A = A - 1
  STA $0200, X          ; Save sprite Y position
  INX
  INX
  INX
  INX
  CPX spnumd2
  BNE BMovePlayer1Up
  RTS

BMovePlayer1Down:
  LDA $0200, X          ; Load sprite Y position
  CLC                   ; Make sure carry flag is set
  ADC #$01              ; A = A + 1
  STA $0200, X          ; Save sprite Y position
  INX
  INX
  INX
  INX
  CPX spnumd2
  BNE BMovePlayer1Down
  RTS

BMovePlayer1Left:
  LDA $0203, X          ; Load sprite X position
  SEC                   ; Make sure the carry flag is clear
  SBC #$01              ; A = A - 1
  STA $0203, X          ; Save sprite X position
  INX
  INX
  INX
  INX
  CPX spnumd2
  BNE BMovePlayer1Left
  RTS

BMovePlayer1Right:
  LDA $0203, X          ; Load sprite X position
  CLC                   ; Make sure carry flag is set
  ADC #$01              ; A = A + 1
  STA $0203, X          ; Save sprite X position
  INX
  INX
  INX
  INX
  CPX spnumd2
  BNE BMovePlayer1Right
  RTS
  
;;----------------------------- PLAYER 2 MOV ----------------------------;;

BMovePlayer2Up:
  LDA $0214, X          ; Load sprite Y position
  SEC                   ; Make sure the carry flag is clear
  SBC #$01              ; A = A - 1
  STA $0214, X          ; Save sprite Y position
  INX
  INX
  INX
  INX
  CPX spnumd2
  BNE BMovePlayer2Up
  RTS

BMovePlayer2Down:
  LDA $0214, X          ; Load sprite Y position
  CLC                   ; Make sure carry flag is set
  ADC #$01              ; A = A + 1
  STA $0214, X          ; Save sprite Y position
  INX
  INX
  INX
  INX
  CPX spnumd2
  BNE BMovePlayer2Down
  RTS

BMovePlayer2Left:
  LDA $0217, X          ; Load sprite X position
  SEC                   ; Make sure the carry flag is clear
  SBC #$01              ; A = A - 1
  STA $0217, X          ; Save sprite X position
  INX
  INX
  INX
  INX
  CPX spnumd2
  BNE BMovePlayer2Left
  RTS

BMovePlayer2Right:
  LDA $0217, X          ; Load sprite X position
  CLC                   ; Make sure carry flag is set
  ADC #$01              ; A = A + 1
  STA $0217, X          ; Save sprite X position
  INX
  INX
  INX
  INX
  CPX spnumd2
  BNE BMovePlayer2Right
  RTS

;;----------------------------- CONTROLLER 1 ----------------------------;;

ReadController1:
  LDA #$01
  STA $4016
  LDA #$00
  STA $4016
  LDX #$08

ReadController1Loop:
  LDA $4016
  LSR A            ; Bit0 -> Carry
  ROL controller1  ; Bit0 <- Carry
  DEX
  BNE ReadController1Loop
  RTS

;;----------------------------- CONTROLLER 2 ----------------------------;;

ReadController2:
  LDA #$01
  STA $4017
  LDA #$00
  STA $4017
  LDX #$08

ReadController2Loop:
  LDA $4017
  LSR A            ; Bit0 -> Carry
  ROL controller2  ; Bit0 <- Carry
  DEX
  BNE ReadController2Loop
  RTS